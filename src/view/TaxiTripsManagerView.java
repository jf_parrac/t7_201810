package view;

import java.util.Scanner;

import controller.Controller;

public class TaxiTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:

					Controller.loadServices( );
					break;
				case 2:
					Controller.ShowTreeInfo();
					
					break;
				case 3:
					System.out.print("Ingrese el ID: ");
					Controller.FindTaxi(sc.next()).toString();
					break;
				case 4:	
					System.out.println("Ingrese la id inferior:");
					String bot=sc.next();
					System.out.println("Ingrese la id superior:");
					for(String t:Controller.IDSRango(bot, sc.next()))
					{
						System.out.println("ID: "+t);
					}
					break;
					
				case 5:
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 6----------------------");
		System.out.println("1. Cargar los servicios de un taxi reportados en un subconjunto de datos");
		System.out.println("2. Mostrar la informacion del arbol");
		System.out.println("3. Mostrar la informacion de un taxi");
		System.out.println("4. Imprimir las id's de un grupo de taxis entre el rango de id dadas");
		System.out.println("5. salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
}
