package controller;

import api.ITaxiTripsManager;
import model.data_structures.ILinkedList;
import model.logic.TaxiTripsManager;
import model.vo.Taxi;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	private static ITaxiTripsManager  manager = new TaxiTripsManager();
	
	/** To load the services of the taxi with taxiId */
	public static void loadServices(  ) {
		// To define the dataset file's name 
		String serviceFile = "./data/taxi-trips-wrvz-psew-subset-medium.json";
		
		manager.loadServices( serviceFile);
	}
	
	
	public static Taxi FindTaxi(String taxid)
	{
		return manager.ShowTaxi(taxid);
	}
	
	public static void ShowTreeInfo()
	{
		manager.showInfo();
	}
	
	public static  ILinkedList<String> IDSRango(String bot,String Top)
	{
		return manager.idRango(bot, Top);
	}
		
	
	
}
