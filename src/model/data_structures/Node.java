package model.data_structures;


public class Node<T extends Comparable<T>>  { 


	private T element;
	private Node<T> prev;
	private Node<T> next;

	public Node(T e, Node<T> p, Node<T> n)
	{
		element = e;
		prev = p;
		next = n;
	}

	public T getElement()
	{
		return element;
	}

	public void setElement(T e)
	{
		element = e;
	}

	public Node<T> getPrev()
	{
		return prev;
	}

	public Node<T> getNext()
	{
		return next;
	}

	public void setPrev(Node<T> p)
	{
		prev = p;
	}

	public void setNext(Node<T> n)
	{
		next = n;
	}

	
	
}
