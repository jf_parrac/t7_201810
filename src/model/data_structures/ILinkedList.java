package model.data_structures;

import java.util.Comparator;
import java.util.Iterator;

public interface ILinkedList<T extends Comparable<T>> extends Iterable<T> {

	int size();

	boolean isEmpty();

	void añadirPrimerElemento(T e);

	//	public void añadirUltimoElemento(T item)
	void añadirUltimoElemento(T e);

	void añadir(T e);

	void añadir(T e, Comparator<T> comp);

	T borrarPrimero();

	T borrarUltimo();

	Node<T> primero();

	Node<T> ultimo();

	T buscarElemento(int i);

	void concatenate(ILinkedList<T> t);

	Node<T> darPrimero();

	void fijarPrimero(Node<T> primero);

	T getElement(int i);

	boolean contains(T nodo);

	Iterator<T> iterator();

	void eliminar(T t);

	T getElement(T t);

	void sort();

}