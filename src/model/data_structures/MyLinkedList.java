package model.data_structures;


import java.util.Comparator;
import java.util.Iterator;


public class MyLinkedList <T extends Comparable<T>> implements Iterable<T>, ILinkedList<T> 
{


	private Node<T> primero;
	private Node<T> ultimo;
	private int size=0;

	public MyLinkedList()

	{
		primero = new Node<T>(null,null,null);
		ultimo = new Node<T>(null,primero,null);
		primero.setNext(ultimo);

	}


	/* (non-Javadoc)
	 * @see model.data_structures.ILinkedList#size()
	 */
	@Override
	public int size()
	{
		return size;
	}


	/* (non-Javadoc)
	 * @see model.data_structures.ILinkedList#isEmpty()
	 */
	@Override
	public boolean isEmpty()
	{
		return size==0;

	}

	/* (non-Javadoc)
	 * @see model.data_structures.ILinkedList#añadirPrimerElemento(T)
	 */
	@Override
	public void añadirPrimerElemento(T e)
	{
		if(size==0)
		{

			primero().setElement(e);
			ultimo.setElement(e);
			size++;

		}		

		else
		{
			Node<T> nuevo = new Node<T>(e,null,primero);
			primero.setPrev(nuevo);
			primero = nuevo;
			size++;


		}


	}




//	public void añadirUltimoElemento(T item)
//	{
//		if( primero == null||primero.getElement()==null)
//			{System.err.println("primero");
//			primero=new Node<T>(item, null, ultimo);
//			size++;
//			}
//		else
//		{
//			if(ultimo==null||ultimo.getElement()==null)
//			{
//				System.err.println("segundo");
//				ultimo.setElement(item);
//				size++;
//			}
//			else {
//				System.err.println("ultimo");
//				ultimo.setNext(new Node<T>(item,ultimo,null));
//				ultimo=ultimo.getNext();
//				size++;
//			}
//		}
//	}
	/* (non-Javadoc)
	 * @see model.data_structures.ILinkedList#añadirUltimoElemento(T)
	 */
	@Override
	public void añadirUltimoElemento(T e)
	{
		if(size==0)
		{

			primero.setElement(e);
			ultimo.setElement(e);
			size++;
		}		

		else 
		{
			Node<T> nuevo = new Node<T>(e,ultimo,null);
			ultimo.setNext(nuevo);
			ultimo = nuevo;
			size++;

		}

	}

	/* (non-Javadoc)
	 * @see model.data_structures.ILinkedList#añadir(T)
	 */
	@Override
	public void añadir(T e)
	{
		if(primero.getElement()==null)
		{
			añadirUltimoElemento(e);
			size++;
			return;
		}
		if(primero.getElement().compareTo(e)<=0)
		{
			Node<T> nuevo = new Node<T>(e,null,primero);
			primero.setPrev(nuevo);
			primero=nuevo;
			size++;
			return;

		}
		Node<T> actual=primero;
		while(actual.getNext()!=null&& actual.getNext().getElement()!=null&&actual.getNext().getElement().compareTo(e)>=0)
		{
			actual=actual.getNext();
		}

		Node<T> nuevo = new Node<T>(e, actual, actual.getNext());

		if(actual.getNext()!=null) {
		actual.getNext().setPrev(nuevo);
		}
		actual.setNext(nuevo);
		size++;

	}
	
	/* (non-Javadoc)
	 * @see model.data_structures.ILinkedList#añadir(T, java.util.Comparator)
	 */
	@Override
	public void añadir(T e,Comparator<T> comp)
	{
		if(primero.getElement()==null)
		{
			añadirUltimoElemento(e);
			size++;
			return;
		}
		if(comp.compare(primero.getElement(), e)<=0)
		{
			Node<T> nuevo = new Node<T>(e,null,primero);
			primero.setPrev(nuevo);
			primero=nuevo;
			size++;
			return;

		}
		Node<T> actual=primero;
		while(actual.getNext()!=null&& actual.getNext().getElement()!=null&&comp.compare(actual.getNext().getElement(), e)>=0)
		{
			actual=actual.getNext();
		}

		Node<T> nuevo = new Node<T>(e, actual, actual.getNext());

		if(actual.getNext()!=null) {
		actual.getNext().setPrev(nuevo);
		}
		actual.setNext(nuevo);
		size++;

	}

	/* (non-Javadoc)
	 * @see model.data_structures.ILinkedList#borrarPrimero()
	 */
	@Override
	public T borrarPrimero()
	{

		Node<T> temp = primero;


		if(primero.getElement()==null)
		{
			return null;

		}

		else if(primero.getNext().getElement()==null)
		{
			primero.setElement(null);
			size--;
			return null;

		}

		else 
		{
			primero.setElement(null);
			primero = temp.getNext();
			size--;
			return (T) primero.getElement();

		}


	}


	/* (non-Javadoc)
	 * @see model.data_structures.ILinkedList#borrarUltimo()
	 */
	@Override
	public T borrarUltimo()
	{

		Node<T> temp = ultimo;

		if(primero.getElement()==null)
		{
			return null;
		}

		else if (primero.getNext().getElement()==null)
		{

			primero.setElement(null);
			size--;
			return null;

		}

		else
		{
			ultimo.setElement(null);
			ultimo = temp.getNext();
			size--;
			return(T) ultimo.getElement();

		}

	}


	/* (non-Javadoc)
	 * @see model.data_structures.ILinkedList#primero()
	 */
	@Override
	public Node<T> primero()
	{
		if(isEmpty())
			return null;

		return primero;

	}




	/* (non-Javadoc)
	 * @see model.data_structures.ILinkedList#ultimo()
	 */
	@Override
	public Node<T> ultimo()
	{
		if(isEmpty())
			return null;

		Node<T> actual=primero;
		while(actual.getNext()!=null)
		{
			actual=actual.getNext();
		}

		return actual;

	}




	/* (non-Javadoc)
	 * @see model.data_structures.ILinkedList#buscarElemento(int)
	 */
	@Override
	public T buscarElemento(int i)
	{
		T encontrado=null;
		int contador= 0;

		Node<T> actual= darPrimero();

		while(encontrado==null && actual!=null)
		{

			if(contador ==i)
			{
				encontrado= actual.getElement();

			}
			contador++;
			actual= actual.getNext();

		}

		return encontrado;
	}

	/* (non-Javadoc)
	 * @see model.data_structures.ILinkedList#concatenate(model.data_structures.ILinkedList)
	 */
	@Override
	public void concatenate(ILinkedList<T> t)
	{
		int i=0;
		for(T e:t)
		{
			if(i==0&&size!=0)
			{
				ultimo.setElement(e);
			}
			else {
				this.añadirUltimoElemento(e);
			}
			i++;
		}
	}


	/* (non-Javadoc)
	 * @see model.data_structures.ILinkedList#darPrimero()
	 */
	@Override
	public Node<T> darPrimero()
	{
		return primero.getNext();
	}





	/* (non-Javadoc)
	 * @see model.data_structures.ILinkedList#fijarPrimero(model.data_structures.Node)
	 */
	@Override
	public void fijarPrimero(Node<T> primero)

	{
		this.primero = primero;

	}



	/* (non-Javadoc)
	 * @see model.data_structures.ILinkedList#getElement(int)
	 */
	@Override
	public T getElement(int i)
	{
		Node<T> actual = primero;
		int j = 0;
		while(actual != null && j != i)
		{
			actual = actual.getNext();
			j++;
		}
	
		return (actual== null)?null:actual.getElement();
	}


	/* (non-Javadoc)
	 * @see model.data_structures.ILinkedList#contains(T)
	 */
	@Override
	public boolean contains(T nodo)
	{		
		for (int i=0,j=size;i<=j;i++,j--)
		{
			if(getElement(i)!=null&&getElement(i).compareTo(nodo)==0)
			{
				return true;
			}
			if(getElement(j)!=null&&getElement(j).compareTo(nodo)==0)
			{
				return true;
			}

		}

		return false;		
	}



	/* (non-Javadoc)
	 * @see model.data_structures.ILinkedList#iterator()
	 */
	@Override
	public Iterator<T> iterator() 
	{
		return new ListaIterable();
	}
	private class ListaIterable implements Iterator<T>
	{

		private Node<T> actual = darPrimero();

		@Override
		public boolean hasNext() 
		{
			return actual!=null;
		}


		@Override
		public T next() 
		{
			T ret = actual.getElement();
			actual = actual.getNext();
			return ret;
		}




	}
	/* (non-Javadoc)
	 * @see model.data_structures.ILinkedList#eliminar(T)
	 */
	@Override
	public void eliminar(T t)
	{
		Node <T>actual=primero;
		while(actual!=null&&actual.getElement()!=t)
		{
			actual=actual.getNext();
		}
		if(actual.getPrev()==null)
		{
			primero=primero.getNext();
			return;
		}
		if(actual.getNext()==null)
		{
			ultimo.getPrev().setNext(null);
			return;
		}
		actual.getPrev().setNext(actual.getNext());
		actual.getNext().setPrev(actual.getPrev());
	}

	/* (non-Javadoc)
	 * @see model.data_structures.ILinkedList#getElement(T)
	 */
	@Override
	public T getElement(T t) {
		
		for(T elemento:this)
		{
			
			if(t.compareTo(elemento)==0)
			{
				return elemento;
			}
		}
		return null;
	}


	/* (non-Javadoc)
	 * @see model.data_structures.ILinkedList#sort()
	 */
	@Override
	public void sort()
	{
		primero=this.mergeSortLinkList(primero);
	}


	private Node<T> mergeSortLinkList(Node<T> startNode){

		//Break the list until list is null or only 1 element is present in List.
		if(startNode==null || startNode.getNext()==null){
			return startNode;
		}

		//Break the linklist into 2 list.
		//Finding Middle node and then breaking the Linled list in 2 parts.
		//Now 2 list are, 1st list from start to middle and 2nd list from middle+1 to last.

		Node<T> middle = getMiddle(startNode);
		Node<T> nextOfMiddle = middle.getNext();
		middle.setNext(null);

		//Again breaking the List until there is only 1 element in each list.
		Node<T> left = mergeSortLinkList(startNode);
		Node<T> right = mergeSortLinkList(nextOfMiddle);

		//Once complete list is divided and contains only single element, 
		//Start merging left and right half by sorting them and passing Sorted list further. 
		Node<T> sortedList = mergeTwoListRecursive(left, right);

		return sortedList;
	}

	//Recursive Approach for Merging Two Sorted List
	private Node<T> mergeTwoListRecursive(Node<T> leftStart, Node<T> rightStart){
		if(leftStart==null)
			return rightStart;

		if(rightStart==null)
			return leftStart;

		Node<T> temp=null;

		if(leftStart.getElement().compareTo(rightStart.getElement())<=0){
			temp=leftStart;
			temp.setNext(mergeTwoListRecursive(leftStart.getNext(), rightStart));
		}else{
			temp=rightStart;
			temp.setNext(mergeTwoListRecursive(leftStart, rightStart.getNext()));
		}
		return temp;
	}

	private Node<T> getMiddle(Node<T> startNode) {
		if(startNode==null){
			return startNode;
		}

		Node<T> pointer1=startNode;
		Node<T> pointer2=startNode;

		while(pointer2!=null && pointer2.getNext()!=null && pointer2.getNext().getNext()!=null){
			pointer1 = pointer1.getNext();
			pointer2 = pointer2.getNext().getNext();

		}
		return pointer1;
	}




}




