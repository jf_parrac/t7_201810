package model.vo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.text.DateFormatter;

/**
 * Modela una rango de fechas y horas (iniciales y finales)
 *
 */
public class RangoFechaHora
{
	//ATRIBUTOS
	
    /**
     * Modela la fecha inicial del rango
     */
	private String fechaInicial; 
	
	/**
	 * Modela la fecha final del rango
	 */
	private String fechaFinal;
	
	/**
	 * modela la hora inicial del rango
	 */
	private String horaInicio; 
	
	/**
	 * modela la hora final del rango
	 */
	private String horaFinal;
	
	private Date inicial;
	
	private Date finalDate;
	
	//CONSTRUCTOR
	/**
	 * @param pFechaInicial, fecha inicial del rango
	 * @param pFechaFinal, fecha final del rango
	 * @param pHoraInicio, hora inicial del rango
	 * @param pHoraFinal, hora final del rango
	 */
	public RangoFechaHora(String pFechaInicial, String pFechaFinal, String pHoraInicio, String pHoraFinal)
	{
		this.fechaFinal = pFechaFinal;
		this.fechaInicial = pFechaInicial;
		this.horaFinal = pHoraFinal;
		this.horaInicio = pHoraInicio;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd,HH:mm");
		try {
			inicial=formatter.parse(fechaInicial+","+horaInicio.substring(0, 4));
			finalDate=formatter.parse(fechaFinal+","+horaFinal.substring(0, 4));
		} catch (ParseException e) {
		
			System.err.println("Error recibiendo las fechas");
			e.printStackTrace();
		}
		
	}
	//M�TODOS
	
	/**
	 * @return the fechaInicial
	 */
	public String getFechaInicial() 
	{
		return fechaInicial;
	}

	/**
	 * @param fechaInicial the fechaInicial to set
	 */
	public void setFechaInicial(String fechaInicial)
	{
		this.fechaInicial = fechaInicial;
	}

	/**
	 * @return the fechaFinal
	 */
	public String getFechaFinal() 
	{
		return fechaFinal;
	}

	/**
	 * @param fechaFinal the fechaFinal to set
	 */
	public void setFechaFinal(String fechaFinal) 
	{
		this.fechaFinal = fechaFinal;
	}

	/**
	 * @return the horaInicio
	 */
	public String getHoraInicio() 
	{
		return horaInicio;
	}

	/**
	 * @param horaInicio the horaInicio to set
	 */
	public void setHoraInicio(String horaInicio) 
	{
		this.horaInicio = horaInicio;
	}

	/**
	 * @return the horaFinal
	 */
	public String getHoraFinal() 
	{
		return horaFinal;
	}

	/**
	 * @param horaFinal the horaFinal to set
	 */
	public void setHoraFinal(String horaFinal) 
	{
		this.horaFinal = horaFinal;
	}
	
	
	public Date getInicial()
	{
		return inicial;
	}
	
	public Date getFinal()
	{
		return finalDate;
	}
	
	public boolean isOnIt(Date time)
	{
		try {
		return(time.before(finalDate)&&time.after(inicial));
		}
		catch(NullPointerException e)
		{
			return false;
		}
	}
}
