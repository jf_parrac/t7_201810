package model.vo;

import java.util.Date;

import jdk.net.NetworkPermission;

public class ClassFactory {





	private String taxi_id;


	private Date trip_end_timestamp;
	
	private Date trip_start_timestamp;

	private String trip_id;
	

	private double trip_miles;

	private int trip_seconds;

	private double trip_total;
	
	private int dropoff_community_area;
	
	private int pickup_community_area;
	
	private String company;
	
	public int getEndArea()
	{
		return dropoff_community_area;
	}
	public String getTaxi_id() {
		return taxi_id;
	}
	public Date getTrip_end_timestamp() {
		return trip_end_timestamp;
	}
	public Date getTrip_start_timestamp() {
		return trip_start_timestamp;
	}
	public String getTrip_id() {
		return trip_id;
	}
	public double getTrip_miles() {
		return trip_miles;
	}
	public int getTrip_seconds() {
		return trip_seconds;
	}
	public double getTrip_total() {
		return trip_total;
	}
	public int getDropoff_community_area() {
		return dropoff_community_area;
	}
	public int getPickup_community_area() {
		return pickup_community_area;
	}
	public String getCompany() {
		return company;
	}
	public int gtStartArea()
	{
		return pickup_community_area;
	}
	public double gtTripDist()
	{
		return trip_miles;
	}


	public Taxi generateTaxi()
	{
		String compania= company==null?"Independ Owner":company;
		return new Taxi(taxi_id,compania);	
	}

	public Service generateService()
	{
		return new Service(trip_end_timestamp, trip_start_timestamp, trip_id, taxi_id, trip_miles, trip_seconds, trip_total, dropoff_community_area,pickup_community_area);
	}
	
	public Company generateCompani()
	{
		if (company!=null)return new Company(company);
		else return new Company("Independent Owner");
	}
	
	


}
