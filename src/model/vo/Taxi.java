package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	public Taxi(String taxi_id,String pCompania) {
		super();
		this.taxi_id = taxi_id;
		this.services = 0;
		this.distanciaRecorrida = 0;
		this.totalWay = 0;
		this.totalWon = 0;
		this.compania=pCompania;
	}

	private String compania;
	
	private String taxi_id;
	
	private int services;
	
	private double distanciaRecorrida;
	
	private double totalWay;
	
	private double totalWon;
		
	
	public String getTaxi_id() {
		return taxi_id;
	}
	
	public void setTaxi_id(String taxi_id) {
		this.taxi_id = taxi_id;
	}
	
	public int getServices() {
		return services;
	}
	
	public void setServices(int services) {
		this.services = services;
	}
	
	public double getDistanciaRecorrida() {
		return distanciaRecorrida;
	}
	
	public void setDistanciaRecorrida(double distanciaRecorrida) {
		this.distanciaRecorrida = distanciaRecorrida;
	}
	
	public double getTotalWay() {
		return totalWay;
	}
	
	public void setTotalWay(double totalWay) {
		this.totalWay = totalWay;
	}
	
	public double getTotalWon() {
		return totalWon;
	}
	
	public void setTotalWon(double totalWon) {
		this.totalWon = totalWon;
	}

	@Override
	public int compareTo(Taxi arg0) {
		
		return 0;
	}
	public String toString()
	{
		return "Taxi id: "+taxi_id+" Serivicios: "+services;
	}
	
	
	public void addService()
	{
		services++;
	}

	public void addWay(double pWay)
	{
	totalWay+=pWay;	
	}
	public void addWon(double pWon)
	{
		
		totalWon+=pWon;
	}
	
}
