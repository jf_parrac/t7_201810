package model.logic;

import java.io.FileReader;
import java.util.Comparator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import api.ITaxiTripsManager;
import model.data_structures.ILinkedList;
import model.data_structures.IRedBlack;
import model.data_structures.MyLinkedList;
import model.data_structures.RedBlackBST;
import model.vo.ClassFactory;
import model.vo.Company;
import model.vo.RangoFechaHora;
import model.vo.Service;
import model.vo.Taxi;

public class TaxiTripsManager implements ITaxiTripsManager {

	
	
	private IRedBlack<String, Taxi> arbol ; 
	
	
	
	
	public void loadServices(String serviceFile) {
		// para probar 28ac0194e9142ad4623904b5724b4ca706200257616e6140f1a7c33266b484b1fecbae4ea46f0f36a69cd91ef201b865a2b07577b3e51007c86b689bfc07a459
		
		
		arbol= arbol==null? new RedBlackBST<>():arbol;


		System.out.println("Inside loadServices with File:" + serviceFile);
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
		try
		{
			ClassFactory[] temp = gson.fromJson(new FileReader(serviceFile),ClassFactory[].class);
			for(ClassFactory dataLoaded:temp)
			{
				if(!arbol.contains(dataLoaded.getTaxi_id()))
				{	
					arbol.put(dataLoaded.getTaxi_id(), dataLoaded.generateTaxi());
					
				}
				arbol.get(dataLoaded.getTaxi_id()).addService();
				arbol.get(dataLoaded.getTaxi_id()).addWay(dataLoaded.getTrip_miles());
				arbol.get(dataLoaded.getTaxi_id()).addWon(dataLoaded.getTrip_total());
				
			}
			
		}
		
		
		

		catch(Exception e)
		{
			System.out.println("Error cargando los servicios");
			e.printStackTrace();
		}
		System.out.println("Inside loadServices with " + serviceFile);



	}
	
	
	
	public void showInfo()
	{
		System.out.println("Total de taxis en el arbol: "+arbol.size());
		System.out.println("Altura (Real) del arbol: "+arbol.height());
		System.out.println("Altura promedio para buscar un taxi existente en el �rbol: "+(((int) Math.log(arbol.size()))+1));
		System.out.println("Altura te�rica del �rbol Red-Black m�s alto que se podr�a construir con el mismo n�merode taxis:"+ (((int)(2*(Math.log(arbol.size()))))+1));//2 lg N
		System.out.println("Altura te�rica del �rbol 2-3 m�s alto que se podr�a construir si se agrega el mismo n�mero de taxis:"+ ((31 - Integer.numberOfLeadingZeros(arbol.size()+1)-1)));//log2(n+1) - 1
		System.out.println("Altura te�rica del �rbol 2-3 m�s bajo que se podr�a construir si se agrega el mismo n�mero de taxis:"+ (log4(arbol.size())));//log4(n+1) - 1
		/*
		 *an�lisis de la altura de su �rbol Red-Black (2.b) con respecto a la altura del �rbol 2.d:
		 *el arbol del taller esta dentro del margen de error de un bst baalanceado respecto a su altura teorica
		 *ventajas:
		 *las busquedas en el peor de los casos reducen su complejidad
		 *desventajas:
		 *----
		 *an�lisis de la altura de su �rbol Red-Black (2.b) con respecto a la altura del �rbol 2.e:
		 *ventajas: el numero de niveles de este arbol es aproximadamente el 66% del construido en el taller, por lo cual las comlejidades se reducen un 33%
		 *desventajas:
		 *el consumo de memoria por las operaciones en un arbol 2-3 no es necesario a cambio de reducir complejidad
		 *an�lisis de la altura de su �rbol Red-Black (2.b) con respecto a la altura del �rbol 2.f:
		 *ventajas: el numero de niveles de este arbol es aproximadamente el 33% del construido en el taller, por lo cual las comlejidades se reducen un 66%
		 *desventajas:
		 *el consumo de memoria por las operaciones en un arbol 2-3 no es necesario a cambio de reducir complejidad
		 */
	}
	//
	private int log4(int size){
		double arriba=Math.log(size);
		double abajo=Math.log(4);
		int rta=(int) (arriba/abajo);
		return rta;
	}
	public Taxi ShowTaxi(String pTaxiId)
	{
		return arbol.get(pTaxiId);
	}

	
	public ILinkedList<String> idRango(String bot,String Top)
	{
		MyLinkedList<String> ids= new MyLinkedList<>();
		
		for(String t:arbol.keysRange(bot, Top))
		{
			ids.a�adirUltimoElemento(t);
		}
		
		
		return ids;
	}

}
