package api;

import model.data_structures.ILinkedList;
import model.vo.Taxi;

/**
 * Basic API for testing the functionality of the TaxiTrip manager
 */
public interface ITaxiTripsManager {

	/**
	 * Method to load the services of a specific taxi 
	 * The services are loaded in both a Stack and a Queue 
	 * @param servicesFile - path to the JSON file with taxi services 
	 * @param taxiId - taxiId of interest 
	 */
	public void loadServices(String serviceFile);
	
	
	
	public void showInfo();
	
	public Taxi ShowTaxi(String pTaxiId);
	
	public ILinkedList<String> idRango(String bot,String Top);



	
}
